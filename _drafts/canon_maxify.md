## Change cartridges

1. open the front door
1. turn off the printer
1. turn on the printer
1. as soon as the print-head cart is moving, unplug the printer
1. rotate the white wheel located in the left side until it clicks
1. move the print-head cart in front of the upper blue button
1. press the upper blue button to remove the old cartridge
1. insert the new cartridge
1. plug the printer
1. turn on the printer

## Hard reset

1. press triangle button
1. while the triangle button is pressed, press the on button
1. release the pressure on the triangle button
1. press 5 times the triangle button
1. wait for the printer to stop making noises
1. press the on button
