> Pour faire taire autrui, commence par te taire.
> Seneque

> Et quand on a pas ce qu'on aime, il faut bien aimer ce qu'on a.
> Corneille

> Si vous voulez que la vie vous sourit, apportez lui d'abord votre bonne humeur.
> Spinoza
