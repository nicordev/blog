---
layout: post
author: nicolas
title: Comment utiliser xdebug avec PHPStorm et docker
tag: 
- fr
---

Xdebug est un outil de step debugging qui peut être utilisé:

1. lorsque nous utilisons un navigateur,

2. lorsque nous exécutons PHP dans un terminal, pour lancer les tests de PHPUnit par exemple.

Dans cet article, nous allons voir comment utiliser xdebug dans ces 2 cas de figure avec PHPStorm et docker, puis nous verrons comment activer xdebug à la volée.

## **Utilisation de xdebug avec un navigateur**

Pour fonctionner, Xdebug doit être installé dans l’image docker, activé dans la configuration de PHP et configuré dans phpstorm.

C’est parti pour l’image docker! En voici une minimale :

```docker
FROM php:8.1-fpm-alpine

RUN set -eux; \
	apk add --no-cache --virtual .build-deps  \
	$PHPIZE_DEPS && \
	pecl install xdebug && \
	docker-php-ext-xdebug.ini && \
	apk del .build-deps

COPY ./devops/php/conf.d/99-xdebug.ini $PHP_INI_DIR/conf.d/99-xdebug.ini

VOLUME /srv/app

WORKDIR /srv/app
```

Xdebug est installé par `pecl install xdebug`, activé avec `docker-php-ext-enable xdebug` et configuré en copiant le fichier de configuration `99-xdebug.ini` dont le contenu est le suivant:

```bash
xdebug.start_with_request=yes

xdebug.mode=debug

xdebug.log=/srv/app/var/log/xdebug.log

xdebug.client_port=9003

xdebug.client_host=host.docker.internal
```

En pratique, la commande `docker-php-ext-enable xdebug` va juste créer un fichier `docker-php-ext-xdebug.ini` dans le dossier `conf.d` de php avec le contenu suivant:

```bash
zend_extension=xdebug
```

Il est donc possible de retirer cette commande `docker-php-ext-enable xdebug` du Dockerfile et d'ajouter `zend_extension=xdebug` dans notre fichier de configuration `99-xdebug.ini` :

```bash
zend_extension=xdebug # activate xdebug extension

xdebug.start_with_request=yes

xdebug.mode=debug

xdebug.log=/srv/app/var/log/xdebug.log

xdebug.client_port=9003

xdebug.client_host=host.docker.internal
```

Ensuite faisons un tour dans la configuration de PHPStorm et ajoutons un serveur :

*File > Preferences > PHP > Servers*

- cliquer sur `+`
- remplir le champ `host` avec l'url de l'application
- cocher `Use path mappings`
- faire correspondre le dossier du projet avec le dossier de l'application sur le serveur

![phpstorm-server-filled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/eb9f179f-70ab-427c-bc85-e80be6df57a5/phpstorm-server-filled.png)

Activer l'écoute en cliquant sur l'icône ressemblant à un téléphone

![phpstorm-telephone.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e7860769-a3e2-4f16-90c0-93c77bc91b3b/phpstorm-telephone.png)

Il reste à placer nos points d'arrêt où nous en avons besoin en cliquant à gauche des lignes de codes:

![phpstorm-breakpoint.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/bd3017b4-dd24-4e81-b02c-1bca8a458106/phpstorm-breakpoint.png)

Voilà ! En naviguant sur notre application, l'éxécution du script va s'arrêter à nos points d'arrêts.

## **Utilisation de xdebug dans un terminal**

Configurer un interpréteur PHP:

*File > Preferences > PHP*

![phpstorm-cli-interpreter-0.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/958b4319-e482-4bcb-b466-3d64726d50be/phpstorm-cli-interpreter-0.png)

- cliquer sur les `...` à droite du champ `CLI Interpreter`
- cliquer sur `+`
- choisir `From Docker, Vagrant, VM, WSL, Remote...`

![phpstorm-cli-interpreter-1.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/3e898950-54bc-48b8-96ad-d531adbff03a/phpstorm-cli-interpreter-1.png)

- choisir le service de l'application
- cliquer sur `Ok`

![phpstorm-cli-interpreter-2.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/df3c7e22-e9fe-4f05-a9eb-79f0f349a2b5/phpstorm-cli-interpreter-2.png)

- dans la zone `General`, cliquer sur les flèches tournantes à droite du champ `PHP executable`, xdebug doit apparaître

![phpstorm-cli-interpreter-3.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/03cb38ac-4d82-41f7-b508-ac9a8ef93d6c/phpstorm-cli-interpreter-3.png)

Ajouter une variable d'environnement `PHP_IDE_CONFIG` :

```bash
docker compose exec --env PHP_IDE_CONFIG=serverName=my-awesome-app.local applicationServiceNameHere
```

Ou de façon plus générale dans le `docker-compose.yaml` :

```yaml
services:
	application:
		environment:
			PHP_IDE_CONFIG: "serverName=${SERVER_NAME}"
```

Où `${SERVER_NAME}` correspond au host du projet, que l'on peut déclarer dans un fichier `.env` à la racine de notre projet:

```yaml
SERVER_NAME=my-awesome-app.local
```

## Charger xdebug à la demande

Le but est de ne charger xdebug que lorsque nous en avons vraiment besoin afin d’améliorer les performances le reste du temps.

L’idée est d’ajouter le paramètre `zend_extension=xdebug` à la volée.

Pour ce faire, retirons le paramètre `zend_extension=xdebug` du fichier de configuration d’xdebug `99-xdebug.ini`:

```bash
xdebug.start_with_request=yes

xdebug.mode=debug

xdebug.log=/srv/app/var/log/xdebug.log

xdebug.client_port=9003

xdebug.client_host=host.docker.internal
```

Et voici comment charger xdebug lorsque nous exécutons une commande, par exemple pour exécuter nos tests phpunit:

```bash
docker compose exec applicationServiceNameHere php --define zend_extension=xdebug.so bin/phpunit
```

Il est aussi possible de lancer nos tests via l’interface de phpstorm :

![Screenshot 2022-11-08 at 22.05.09.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/6823231d-1ef5-4a13-a621-a1c4bbe59f95/Screenshot_2022-11-08_at_22.05.09.png)

Dans ce cas, il s’agit d’ajouter `xdebug` dans *File > Preferences > PHP > CLI Interpreter - ... > Additional - Debugger extension* :

![Screenshot 2022-11-08 at 22.12.50.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/cf476522-401a-4bcb-b519-83ff1c1a1e09/Screenshot_2022-11-08_at_22.12.50.png)

Pour ce qui est de l’utilisation d’un navigateur, WIP