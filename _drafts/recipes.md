## fondant chocolat

1. fondre 200g chocolat, 100g beurre
1. séparer le blanc du jaune de 3 oeufs
1. battre les blancs en neige
1. mélanger dans un saladier chocolat fondu, jaunes d'oeufs, 100g sucre, 50g farine
1. ajouter les blancs en neige
1. verser dans un moule beurré
1. cuire 20' à 180 °C (th. 6)

## muffins fourrés

1. fondre 100g chocolat, 50g beurre
1. mélanger dans un saladier 3 oeufs, 80g sucre
1. ajouter 1 cs farine
1. ajouter le chocolat fondu
1. beurrer un moule à muffin
1. verser 1/3 du mélange dans le moule
1. déposer 2 carrés de chocolat dans chaque moule et recouvrir avec le reste du mélange
1. cuire 8' à 240 °C (th. 8)

## muffins marbrés

1. fondre 100g chocolat
1. fondre 140g beurre
1. mélanger dans un saladier 4 oeufs, 80g sucre, 140g farine, 1 sachet de levure
1. ajouter le beurre fondu
1. ajouter le chocolat dans 1/2 du mélange
1. ajouter 1/2 cc d'extrait d'amandes amères dans l'autre 1/2 du mélange
1. verser en alternant chaque mélange dans un moule à muffin jusqu'aux 2/3 de la hauteur du moule
1. cuire 18' à 200 °C (th. 6-7)

## Fraisier

> [Source](https://www.hervecuisine.com/recette/fraisier-facile-et-leger-avec-herve-cuisine/)

**le sirop**
1. mélanger dans une casserole:
    - 20 cl d'eau
    - 150 g de sucre
    - une gousse de vanille fendue
1. chauffer à ébullition 2-3 minutes puis laisser infuser

**la crème patissière**
1. chauffer dans une casserole:
    - 1 gousse de vanille fendue
    - 25 cl de lait
1. retirer la gousse de vanille de la casserole
1. ajouter 1 g d'agar agar (ou 2 feuilles de gelatine ramollies dans de l'eau froide)
1. faire bouillir 1 minute
1. mélanger dans un saladier:
    - 2 jaunes d'oeufs
    - 50 g de sucre
    - 40 g de maïzena
1. verser le saladier dans la casserole en remuant avec un fouet
1. faire chauffer à feu moyen la casserole tout en remuant avec un fouet pour épaissir la crème
1. mettre la crème dans un récipient recouverte de film alimentaire au contact de la crème
1. mettre le récipient au frigo pendant plusieurs heures

**la crème fouettée**
1. mettre 20 cl de crème liquide entière dans un récipient avec les 2 fouets du batteur 15 minutes dans le congélateur
1. fouetter la crème liquide entière

**la crème**
- mélanger au batteur 1 cuillère à soupe de crème fouettée dans la crème patissière
- mélanger le reste de la crème fouettée dans la crème patissière délicatement avec une marise
- mettre la crème dans une poche à douille

**la génoise**
- battre 4 blancs d'oeufs en neige
- ajouter 50 g de sucre dans les blancs en neige
- battre de nouveau
- fondre 50 g de beurre dans une casserole
- mélanger dans un saladier:
    - 4 jaunes d'oeufs
    - 100 g de sucre
    - des graines d'une gousse de vanille ou un sachet de sucre vanillé
    - le beurre fondu
    - 50 g de farine
    - 2 cuillères à soupe de blancs en neige
    - 50 g de farine
    - le reste des blancs d'oeufs délicatement avec un fouet
- mettre du papier cuisson et des cercles en métal sur 2 plaques
- verser le mélange dans les cercles
- faire chauffer les plaques au four 10 minutes à 190 °C
- imbiber avec un pinceau les génoises avec le sirop

**le fraisier**
- découper 250 g à 500 g de fraises en 2 en leur donnant la même hauteur
- disposer les fraises autour d'une génoise dans son cercle de cuisson
- verser la crème avec la poche à douille en formant de petits tas contre les fraises puis au centre
- disposer des fraises entre les tas
- verser le reste de crème
- recouvrir avec le 2ème disque de génoise
- appuyer sur le dessus
- ajouter de la pâte d'amande

à acheter:
- 2 cercles de cuisson en métal
- 1 petit fouet
- film alimentaire
- 500 g de fraises
- 3 gousses de vanille
- 6 oeufs
- sucre
- maïzena
- sachet d'agar agar
