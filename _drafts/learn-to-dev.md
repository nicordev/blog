Pourquoi les développeurs échouent :
 
- Lisent la description de la tâche
- Commencent à écrire le code
- Rencontrent un problème
- Cherchent de trouver la meilleur solution
- Copient et collent le code trouvé sur le net
- Se retrouvent à nouveau bloqué
- Effacent tout
- Repartent de zéro
- Demandent de l'aide...
 
Comment les développeurs performants résolvent les problèmes :
 
- Lisent la description de la tâche
- Réfléchissent à plusieurs solutions
- Choisissent la meilleure solution
- Élaborent un bon plan
- Écrivent les tests (TDD)
- Écrivent le code
- Écrivent encore des tests
- Refactorisent
- Optimisent