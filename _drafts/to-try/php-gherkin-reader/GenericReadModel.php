<?php

declare(strict_types=1);

namespace App\Tests\Behat\Core\Http\AbstractContext\ReadModel;

use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;

final class GenericReadModel extends AbstractGenericReadModel
{
    /**
     * @Then response contains read model list with:
     */
    public function thenList(TableNode $inputs): void
    {
        $expectedInputs = $this->convertInputs($inputs->getRowsHash());

        $response = $this->getResponse()->toArray(false);
        Assert::assertArrayHasKey('hydra:member', $response);

        $elements = $response['hydra:member'];

        $this->assertElementCompliance(
            $expectedInputs,
            reset($elements),
        );
    }

    /**
     * @Then response contains read model with:
     */
    public function thenSingle(TableNode $inputs): void
    {
        $expectedInputs = $this->convertInputs($inputs->getRowsHash());

        $response = $this->getResponse()->toArray(false);
        $this->assertElementCompliance(
            $expectedInputs,
            $response,
        );
    }
}
