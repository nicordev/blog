<?php

declare(strict_types=1);

namespace App\Tests;

final class SingleExpectedValue
{
    private function __construct(
        public readonly string $value,
        public readonly string $type,
    ) {
    }

    public static function createFromInputs(array $inputValues): self
    {
        return new self(
            $inputValues[0],
            $inputValues[1],
        );
    }
}
