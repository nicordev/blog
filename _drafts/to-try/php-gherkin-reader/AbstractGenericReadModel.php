<?php

declare(strict_types=1);

namespace App\Tests\Behat\Core\Http\AbstractContext\ReadModel;

use App\Core\Domain\ValueObject\DateTime;
use App\Tests\Behat\Core\Http\BaseContext;
use App\Tests\IdentifierResolver;
use App\Tests\SingleExpectedValue;
use PHPUnit\Framework\Assert;

abstract class AbstractGenericReadModel extends BaseContext
{
    protected function convertInputs(array $inputs): array|SingleExpectedValue
    {
        $output = [];
        foreach ($inputs as $key => $value) {
            $keyParts = explode('.', $key);
            $outputRef = &$output;
            foreach ($keyParts as $part) {
                if (array_key_exists($part, $outputRef) === false) {
                    $outputRef[$part] = [];
                }

                $outputRef = &$outputRef[$part];
            }

            $outputRef = SingleExpectedValue::createFromInputs($value);
        }

        return $output;
    }

    protected function assertElementCompliance(array $input, array $element): void
    {
        foreach ($element as $fieldName => $value) {
            // skip APP fields
            if (str_starts_with($fieldName, '@')) {
                continue;
            }

            // check relationships
            if (is_array($value) === true) {
                $this->assertElementCompliance(
                    $input[$fieldName],
                    $value,
                );
                unset($input[$fieldName]);
                continue;
            }

            // gracefully skip empty relationships
            if ($value === null && is_array($input[$fieldName]) === true) {
                $this->assertChildrenAreNull(
                    $input[$fieldName],
                    $fieldName
                );
                continue;
            }

            // perform check
            $this->assertSingleValue(
                $input[$fieldName],
                $value,
                $fieldName,
            );

            // unset checked value
            unset($input[$fieldName]);
        }

        Assert::assertEmpty(
            $input,
            'Extra values present in scenario that were not found in response values: '.implode(', ', array_keys($input))
        );
    }

    private function assertSingleValue(SingleExpectedValue $expectedValue, mixed $value, string $fieldName): void
    {
        switch ($expectedValue->type) {
            case 'date':
                Assert::assertStringStartsWith($expectedValue->value, $value, 'Failed matching date on field '.$fieldName);

                break;
            case 'date_format':
                Assert::assertSame(
                    (new DateTime($expectedValue->value))->format(DateTime::DATE_FORMAT),
                    (new DateTime($value))->format(DateTime::DATE_FORMAT),
                    'Failed matching date_format on field '.$fieldName,
                );

                break;
            case 'file':
                Assert::assertStringEndsWith($expectedValue->value, $value, 'Filename not found in field '.$fieldName);

                break;
            case 'signed_file':
                Assert::assertContains('Expires', $value, '?Expires not found for file '.$fieldName.'. Is the file properly secured?');
                Assert::assertContains('Signature', $value, '?Signature not found for file '.$fieldName.'. Is the file properly secured?');
                Assert::assertContains($expectedValue->value, $value, 'Filename not found in field '.$fieldName);

                break;
            case 'ulid':
                Assert::assertSame(IdentifierResolver::ulid($expectedValue->value)->value(), $value);

                break;
            case 'string':
                Assert::assertSame($expectedValue->value === "" ? null : $expectedValue->value, $value);

                break;
            case 'int':
                Assert::assertEquals($expectedValue->value, $value);

                break;
            case 'null':
                Assert::assertNull($value);

                break;
            case 'bool':
                Assert::assertSame(
                    filter_var($expectedValue->value, FILTER_VALIDATE_BOOL),
                    $value,
                );

                break;
            case 'skip':
                break;
            default:
                throw new \Exception('Unexpected field type "'.$expectedValue->type.'" in scenario for field: "'.$fieldName);
        }
    }

    /**
     * @param array<SingleExpectedValue> $inputs
     */
    private function assertChildrenAreNull(array $inputs, string $fieldName): void
    {
        foreach ($inputs as $children) {
            Assert::assertNull($children->value === "" ? null : $children->value, 'Failed asserting relationship "'.$fieldName.'" contains no data');
        }
    }
}
