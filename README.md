# My static blog

- [Blog](https://nicordev.gitlab.io/blog)
- [Gitlab pages from scratch](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)
- [Jekyll](https://jekyllrb.com/)
- [Bulma CSS](https://bulma.io/documentation/)

## Installation

```
make install
```

## Usage

```
make serve
```