---
layout: post
author: nicolas
title: Apprendre le développement web
excerpt_separator: <!-- excerpt end -->
categories: web
tags:
- fr
---

Sites contenant uniquement des cours gratuits:
- [FreeCodeCamp](https://www.freecodecamp.org/)
- [OpenClassrooms](https://openclassrooms.com/fr/)

<!-- excerpt end -->

En gros le développement web est souvent représenté en 2 partie:
- backend: code exécuté côté serveur, partie invisible d'un site web (gestion de la base de données, gestion des autorisations, API utilisés par d'autres programmes)
- frontend: code exécuté côté client, c'est la partie visible du site web (interface, design)

Le terme fullstack regroupe à la fois le backend et le frontend.

Quelques conseils pour débuter :
- L'important n'est pas de suivre tel ou tel cours, mais plutôt de pratiquer. Le plus efficace : créer ses propres projets en parallèle des cours, c'est beaucoup plus motivant quand tu trouves un intérêt concret à apprendre ces technologies.
- Au début c'est normal de ramer, ça passe avec la pratique.
- En cas de soucis, google est ton ami.
- Lire la documentation (par exemple celle du Mozilla Developper Network est superbe : https://developer.mozilla.org/en-US/)
- Nommer ses variables/fonctions de façon précise et détaillée, vaut mieux un nom long et explicite qu'un mot court mais vague.
- Découvrir les principes SOLID pour cesser de perdre du temps à coder dans le vent (rien que d'appliquer le S, c'est déjà très efficace)
- Abandonner les croyances comme quoi ça serait complexe et hors de portée, car si c'est le cas, c'est juste que c'est mal codé.
- Utiliser un gestionnaire de dépendances (par exemple composer pour PHP)
- Après quelques temps, créer des sites en utilisant un framework (par exemple Symfony pour PHP)
- Utiliser git
- Utiliser le terminal Linux
- Remplacer windows par une distribution Linux
- Ecrire des tests unitaires et des tests d'intégration (par exemple en utilisant phpunit pour PHP)
- Une fois capable de créer des applications, utiliser docker

Pour info, voici dans l'ordre chronologique les cours gratuits que j'ai suivi quand je me suis mis à apprendre à coder :
- [Le langage C](https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c) (les 2 premières parties suffisent, ça te permet de comprendre des concepts implicitement utilisés par d'autres langages comme les pointeurs ou la gestion de mémoire)
- [Comprendre le web](https://openclassrooms.com/fr/courses/1946386-comprendre-le-web)
- [Apprenez à créer votre site web avec HTML5 et CSS3](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3)
- [Apprenez à coder avec JavaScript](https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript) (j'avais suivi l'ancienne version, là c'est la nouvelle)
- [Ecrivez du JavaScript pour le web](https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web) (j'avais suivi l'ancienne version, là c'est la nouvelle)

La formation que j'ai suivi: [Développeur d'application - PHP/Symfony](https://openclassrooms.com/fr/paths/500-developpeur-dapplication-php-symfony)

Des outils gratuits pour commencer à coder:

Editeur de texte:
- [VSCodium](https://vscodium.com/)
- [codepen](https://codepen.io/)

Gestion de code source:
- [git](https://git-scm.com/)
- [github](https://github.com/)
- [gitlab](https://gitlab.com/users/sign_in)

Base de données:
- [DBeaver](https://dbeaver.io/)

**Enjoy!**