---
layout: post
author: nicolas
title: Web dev tips
categories: web
excerpt_separator: <!-- excerpt end -->
---

- [Use User Research to Create the Perfect UI Design](https://www.freecodecamp.org/news/use-user-reseach-to-create-the-perfect-ui-design/)
- [Top JavaScript Concepts to Know Before Learning React](https://www.freecodecamp.org/news/top-javascript-concepts-to-know-before-learning-react/)
- [Best practices for react](https://www.freecodecamp.org/news/best-practices-for-react/)

## Tools

- [pikist - free pictures](https://www.pikist.com/fr)
- [icons8 - free icons](https://icons8.com/)
- [traefik](https://traefik.io/)
- [flatlogic](https://flatlogic.com/?utm_medium=email&utm_campaign=website&utm_term=platform_updates&utm_source=sendgrid)
