---
layout: post
author: nicolas
title: Symfony basics
categories: php
excerpt_separator: <!-- excerpt end -->
---

Some Symfony basics

<!-- excerpt end -->

## Services

Given this configuration:

```yaml
services:
    _defaults:
        autowire: true
        autoconfigure: true
        public: false

    fruit.order_filter:
        parent: 'api_platform.doctrine.orm.order_filter'
        arguments:
        -
            id: ~
            name: ~
            updatedAt: ~
            createdAt: ~
        tags:
        - 'api_platform.filter'
        autowire: false
        autoconfigure: false
        public: false
```

Our fruit order filter service extends another existing service using tags.

List all services inside the dependency injection container:

```php
php bin/console debug:container
```

Get details on a service:

```php
php bin/console debug:container serviceNameHere
```

## Autodiscovery

Given this configuration:

```yaml
services:
    App\:
        resource: '../src/*'
        exclude: '../src/{DependencyInjection,Entity,Migrations,Tests,Kernel.php,Client}'
```

In development, at each HTTP request, Symfony will hash each files under the `src/` directory using md5 except those defined in `exclude`, then compare them with the previously generated cache.

If one hash is different than the previous one, Symfony will create a new cache by generating one php file for each service.

If a service is not used for the request, Symfony will remove the file.

As the project grow, it will take more and more time to do this autodiscovery thing.

To avoid this, explicitly declare each service, like that:

```yaml
    App\DataFixtures\Faker\Provider\IndexProvider: ~
    App\DataFixtures\Faker\Provider\ValueOrNullProvider: ~
```
