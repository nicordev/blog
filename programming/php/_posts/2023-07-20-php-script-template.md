---
layout: post
author: nicolas
title: Template de script php
categories: php
excerpt_separator: <!-- excerpt end -->
tags:
- fr
---

<!-- excerpt end -->

voici l'équivalent en PHP de mon template de script bash:

> [fichier](https://gitlab.com/nicordev/cli/-/blob/master/script/php/please/please.php?ref_type=heads)

```php
#! /usr/bin/env php
<?php

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));
define('FUNCTION_NAME', array_shift($argv));

function say_hello(array $arguments)
{
    if (count($arguments) < 1) {
        echo sprintf(<<< MESSAGE

            %s %s\033[33m name\033[0m
            \n
            MESSAGE,
            SCRIPT_NAME,
            FUNCTION_NAME,
        );

        return;
    }

    $name = $arguments[0];

    echo "Hello {$name}!\n";
}

function how_it_works()
{
    echo _get_script_content();
}

function _get_script_content()
{
    return file_get_contents(__DIR__.'/'.SCRIPT_NAME);
}

function _list_actions()
{
    $content = _get_script_content();
    $matches = [];
    preg_match_all('#^function ([a-z]{1}[a-zA-Z0-9_]*)\(#m', $content, $matches);
    $actions = $matches[1];
    asort($actions);

    return $actions;
}

if ($argc <= 1) {
    echo implode("\n", _list_actions())."\n";
    exit;
}

$actions = _list_actions();

if (!in_array(FUNCTION_NAME, $actions)) {
    echo "Unknown action ".FUNCTION_NAME."\n";
    exit;
}

call_user_func(FUNCTION_NAME, $argv);
```

même logique, les fonctions commençant par `_` seront absente de la liste des fonctions disponible.

pour l'installer:

1. créer un fichier contenant le script, par exemple `my_script`
1. rendre le fichier exécutable:

    ```sh
    chmod +x ./my_script
    ```

pour l'utiliser:
1. lancer le script

    ```sh
    ./my_script
    ```

    va donner la liste des fonctions:

    ```
    howItWorks
    sayHello
    ```
1. relancer le script avec la fonction voulu pour connaître les paramètres à utiliser

    ```sh
    ./my_script sayHello
    ```

    va donner:

    ```
    my_script sayHello name
    ```
1. relancer le script avec les paramètres

    ```sh
    ./my_script.php sayHello world
    ```

    va donner:

    ```
    Hello world!
    ```

Voilà! vous pouvez ajouter les fonctions que vous souhaitez, modifier celle existante, comme vous voulez.
