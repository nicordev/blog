---
layout: post
author: nicolas
title: Install php modules
categories: php
excerpt_separator: <!-- excerpt end -->
---

- sqlite

<!-- excerpt end -->

## ubuntu

sqlite

```sh
sudo apt-get install php-sqlite3
```

postgresql

```sh
sudo apt-get install php-pgsql
```

## list installed modules

```sh
php -m
php --modules
```
