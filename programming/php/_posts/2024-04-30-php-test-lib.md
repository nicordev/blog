---
layout: post
author: nicolas
title: Mini lib de test php maison
categories: php
excerpt_separator: <!-- excerpt end -->
tags:
- fr
---

<!-- excerpt end -->

voici le fichier ce que j'utilise pour écrire des tests rapidement sans phpunit ni composer:

> [fichier](https://gitlab.com/nicordev/learn_php/-/blob/master/test-runner/test.php?ref_type=heads)

il contient une fonction `test` qui s'utilise un peu comme jest ou pest, on passe une description et un callback à la fonction test et c'est parti !

j'y ai aussi mis une classe pour faire des assertions, un peut comme le `$this->assertSame()` de phpunit en moins verbeux:

```php
it:is_same($expected, $actual, $some_optional_message_on_failure);
```

et on peut charger notre autoloader en modifiant cette ligne du fichier test.php:

```php
// autoloader
// require_once __DIR__.'/autoload.php';
```

exemple de fichier de test:

```php
<?php

declare(strict_types=1);

use function test\test;
use test\it;
use test\test_exception;

// the class we want to test
use greet\greeter;

test(
    description: 'can greet',
    test: function (string $input_name, string $expected_name) {
        $greeter = new greeter();

        $result = $greeter($input_name);

        // either throw yourself some exceptions
        if ($result !== "Hello $expected_name!") {
            throw new test_exception('Wrong greet.');
        }

        // or use the "it" class
        it::is_same("Hello $expected_name!", $result);
    },
    cases: [
        ['banana', 'banana'], // here are the parameters provided to the test
        ['orange', 'orange'],
        // ['orange', 'broken'], // this one will fail
    ]
);

test(
    description: 'failing test',
    test: function () {
        it::is_same(1, 2);
    },
    cases: []
);
```

pour lancer les tests:

```sh
php test.php my_test_directory_here
```

et voilà!