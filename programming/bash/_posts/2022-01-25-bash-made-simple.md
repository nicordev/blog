---
layout: post
author: nicolas
excerpt_separator: <!-- excerpt end -->
---

A simple way of writing bash script without any "magic" or hassle.
<!-- excerpt end -->

How I do:
- display how to use the command when you call the command without any parameter
- use positional parameters
- set variables before calling the command
- stay consistent
