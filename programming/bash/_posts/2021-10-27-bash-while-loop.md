---
layout: post
author: nicolas
---

<!-- excerpt end -->

Also you can write directly the condition in the while statement:

```bash
i=0

while [ $i -lt 5 ]
do
    echo hello
    ((i++))
done

echo 'World! have a nice day!'
```

Sometime the condition can be hard to write and to read.

To avoid the hassle of writing a proper condition in my `while` loops, here is what I do:

```bash
yourConditionHere() {
    # your condition here
}

while true
do
    if yourConditionHere
    then
        # jump to the next iteration:
        continue
    fi

    # exit the loop:
    break
done
```

Example:

```bash
isIterationLowerThan5() {
    [ $i -lt 5 ]
}

i=0

while true
do
    echo hello
    ((i++))

    if isIterationLowerThan5
    then
        continue
    fi

    break
done

echo 'World! have a nice day!'
```

Just remember, `continue` will jump to the next loop iteration while `break` will stop and exit the loop.

*Have fun!*