---
layout: post
author: nicolas
title: Bash history tip
excerpt_separator: <!-- excerpt end -->
---

The `history` command keeps track of each command you enter in the terminal.

<!-- excerpt end -->

Also when you use the up arrow key, it will give you the previous commands you entered.

To enter a command without having it recorded in `history` or being able to retrieve it using the up arrow key, add a space in front of your command like so:

```bash
echo "This command will be remembered"
 echo "This one won't"
echo "This last one will be remembered"
```