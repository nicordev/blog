---
layout: post
author: nicolas
title: Code review
categories: programming
---

Using [conventional comments](https://conventionalcomments.org/).

<!-- excerpt end -->

- [method](https://conventionalcomments.org/)
- [demo (fr)](https://www.youtube.com/watch?v=LVh6iQtJW2I)
- [gitlab extension](https://gitlab.com/conventionalcomments/conventional-comments-button)