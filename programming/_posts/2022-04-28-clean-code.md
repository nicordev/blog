---
layout: post
author: nicolas
title: Clean code
categories: programming
---

<!-- excerpt end -->

- [clean code in video](https://cleancoders.com/episode/clean-code-episode-1)
- [book to buy](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
- [book to read](https://drive.google.com/file/d/0B1lMPPfEr07IV1VqVFFJaE5ZbTA/preview?resourcekey=0-kpoviTbHIbX5A4_A46X79g)
