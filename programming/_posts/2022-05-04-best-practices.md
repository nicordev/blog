---
layout: post
author: nicolas
title: Clean code
categories: programming
---

<!-- excerpt end -->

- [never use null](https://qafoo.com/blog/083_never_use_null.html)
- [design classes](http://bestpractices.thecodingmachine.com/php/design_beautiful_classes_and_methods.html)

- [validation in domain](https://groups.google.com/g/clean-code-discussion/c/latn4x6Zo7w/m/bFwtDI1XSA8J)

    > This is really just the SRP / CCP again. Gather together things that change for the same reasons.  Separate things that change for different reasons.
