---
layout: post
author: nicolas
title: Rénovation d'une ferme en pierre et pisé
category: autonomy
tags:
- fr
excerpt_separator: <!-- excerpt end -->
---

- [cordage pour plinthe](https://www.cordagesgautier.com/cordage-torone-en-chanvre.html)

<!-- excerpt end -->

## Etapes

1. [x] rendez-vous avec un architecte pour étudier la faisabilité vis-à-vis de notre budget
1. [x] simulations de prêts bancaires
1. [x] signature du compromis de vente
1. [x] création des plans pour le permis de construire (ouvertures dans les murs, toiture, chemin d'accès)
1. [x] dépôt du permis de construire
1. [x] réalisation de devis pour les différents corps de métier
1. [x] trouver assurance pour le prêt
1. [x] trouver une assurance dommage ouvrage (nécessaire pour l'obtention du prêt)
1. [x] dépôt du dossier de demande de prêt à la banque
1. [x] 2022-01-27 signature de l'achat de la maison
1. [x] prendre rendez-vous avec enedis pour poser le compteur électrique de chantier
1. [x] prendre rendez-vous avec suez pour poser le compteur d'eau
1. [x] obtenir un marteau piqueur
1. [ ] obtenir une remorque
1. [x] sonder les fondations du sous-sol
1. [x] sonder les fondations de l'entrée
1. [x] démolir la dalle de béton de l'entrée
1. [x] démolir la dalle de béton du sous-sol
1. [x] couper le lierre
1. [x] évacuer les gravats
1. [x] décaisser sous-sol
1. [x] décaisser bureau
1. [x] balayer plancher RDC
1. [x] faire une étude de structure (nécessaire pour l'obtention de l'assurance dommage ouvrage)
1. [x] 2022-02-01 rendez-vous thermicien
1. [x] définir les niveaux définitifs des planchers avant le 2022-02-11
1. [x] 2022-02-11 déterminer les côtes avec le maçon
1. [x] prévoir réunion de chantier début mars
1. [x] réunion de chantier le 2022-03-04
1. [x] mettre des pierres de côté
1. [x] choisir revêtement salle de bain
1. [x] choisir robinetterie
1. [x] choisir disposition sous-sol
1. [x] acheter ponceuse
1. [x] acheter aspirateur
1. [x] acheter projecteur
1. [x] creuser coin Sud-Est
1. [ ] fabriquer escalier extérieur amovible

## Journal

- 01/02/2022 30min démolition de la dalle de l'entrée à la barre à mine
- 05/02/2022 2h démolition de la dalle de l'entrée à la barre à mine
- 18/02/2022 une après-midi à 2 pour:
    - démolir une partie de la dalle du sous-sol
    - balayer le plancher existant
    - évacuer les débris de l'entrée
    - évacuer des débris du sous-sol
    - démolir le béton des murs de l'entrée
- 19/02/2022 un samedi entier à 2 pour:
    - démolir tout le restant de la dalle du sous-sol
    - démolir le béton des murs de l'entrée
- 20/02/2022 un dimanche pour:
    - démolir le béton des murs de l'entrée

## Matériel

- échelle
- casque anti-bruit
- bouchons anti-bruits
- lunettes de protection
- enrouleur
- pioche
- pelle
- brouette
- barre à mine
- bâche
- massette
- burin
- pied de biche
- masse
- marteau piqueur
- perforateur

## Murs en pierre et pisé

Les murs en pisés sont des murs en terre posés sur des murs en pierre. La pierre permet d'éviter que l'humidité du sol ne remonte dans le pisé.

La pierre est dense, ce qui en fait un excellent isolant phonique et un mauvais isolant thermique.

Les murs en pisé perspirent, c'est à dire qu'ils régulent l'humidité et la température de la maison. Ils gonflent ou se rétracte selon les conditions météo, comme le bois.

Le pisé a des propriétés intéressantes, notamment en terme d'isolation thermique et de gestion de l'humidité de la maison. Pour pouvoir en bénéficier, il est nécessaire de mettre de l'enduit chaux/chanvre et/ou un système d'isolation respirant pour éviter que le mur ne s'abîme et pour permettre de garder les propriétés du pisé.

Il est préférable de mettre de l'enduit chaux/chanvre ou un système d'isolation respirant pour éviter que le mur ne s'abîme et pour permettre de garder les propriétés du pisé.

Éviter de mettre une ceinture en béton armé sur un mur en pisé car cela risque de dégrader le mur à la longue, vu que le béton est un matériau qui n'a aucune flexibilité, contrairement au pisé. Préférer du bois qui va aussi bouger avec le mur.

## Fenêtres

Matériaux possibles:
- bois: le moins cher
- aluminium
- bois à l'intérieur et aluminium à l'extérieur: le plus cher

Nous optons pour le bois.

Des ouvertures seront à créer dans le mur en pisé.

Un cadre en bois sera installé dans chaque ouverture pour permettre de poser la fenêtre.

Au sommet de la baie vitrée de la cuisine, installer une plaque recouverte d'enduit pour combler l'espace libre.

## Portes

Remplacement de la partie basse, abîmée, du cadre de la porte d'entrée par des pierres.

## Charpente

Afin de permettre la création des chambres à l'étage, les fermes triangulaires vont être découpées pour laisser un passage au centre.

Des jambes de force en appuis sur les poutres du plancher vont être installées et une pièce de bois horizontale va être placée en haut de chaque ferme pour répartir les forces.

Afin de garder une hauteur sous plafond suffisante, l'isolation du toit sera faite par l'extérieur avec la technique du sarking. Dans l'idéal, l'isolant utilisé sera de la laine de bois sur 30 cm d'épaisseur ou une couche de 6 cm de laine de bois placée sur 24 cm de ouate de cellulose.

L'isolation en sarking est beaucoup plus efficace qu'une isolation sous toit.

Les chevrons existants seront coupé au raz du mur et d'autres seront placés au-dessus de l'isolation.

Des planches combleront l'espace entre le mur et les nouveaux chevrons. *Comment placer de l'isolant entre ces planches et les plaques intérieures?*

Les chevrons existants seront scellés avec un béton particulier (*lequel?*).

## Dalle au rez-de-jardin

Composition prévue de bas en haut:
1. hérisson ventilé
1. béton de chaux
1. isolant en plaques de liège
1. (optionnel) tuyaux du chauffage au sol
1. chape de chaux (5cm)
1. tommettes en terre cuite scellées dans la chape ou travertin collé sur la chape sèche

L'installation d'une dalle en béton de chaux sur un hérisson ventilé avec tommettes ou travertin permettra à l'humidité du sol de remonter sans abîmer les murs, contrairement au béton et carrelage classiques qui ne laissent rien passer.

Pose de tomettes:
- nécessite de laisser tremper les tomettes de 24h pour des tomettes récentes à plusieurs jours pour des tomettes anciennes
- [conseils de pose](https://www.terres-cuites-raujolles.fr/fr/conseils-pose-terres-cuites/techniques-pose-tomettes)

Achat de tomette:
- [raujolles](https://www.terres-cuites-raujolles.fr/fr/25-sol-interieur)

Pose de travertin:
- [conseils de pose](https://www.espace-travertin.fr/poser-le-travertin/)
- [vidéo de pose](https://www.youtube.com/watch?v=Eyqxm4PhgBM)

## Chauffage

Poêle:
- nécessite un conduit de cheminée qui dépasse de 40 cm au-dessus du faîtage du toit, il est préférable de le placer au centre pour éviter d'avoir un conduit qui dépasse trop du toit, nécessitant des amarres.
- le placer contre un mur en pisé pour que le mur puisse restituer la chaleur.
- modèles:
    - [nordica helga-evo](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/helga-evo)
    - [nordica marlena](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/marlena)
    - [godin castries](https://www.godin.fr/products/castries/)
    - [godin montanay](https://www.godin.fr/products/montanay/)
    - [godin camelia](https://www.godin.fr/products/camelia/)
- modèles permettant de bouillir de l'eau sur le dessus:
    - [nordica concita](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/concita) 11.1 kW avec prise directe d'air externe BBC
    - [nordica TermoRossella](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/termorossella-plus-evo-dsa-40) 11.1 kW avec prise d'air externe ASC
    - [nordica gemma](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/gemma) 6 kW sans prise d'air extérieure
    - [nordica Rossella Plus](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/rossella-plus) 8 kW sans prise d'air externe
    - [nordica Emiliana](https://www.lanordica-extraflame.com/fr/produits/poeles-bois/emiliana) 6.5 kW sans prise d'air externe

Poêle à granulée:
- nécessite une bâche volumineuse pour contenir le stock de granulé pour l'année ou d'utiliser 1 sac par jour

Plancher chauffant:
- à eau
- à résistance électrique

Sites marchands:
- [matériaux naturels](https://www.materiaux-naturels.fr/search?q=plancher%20chauffant)

Plancher chauffant électrique:
- 7000 W ou 5000 W avec foisonnement
- à champs électromagnétique réduit:
    - [champs électromagnétique](https://www.ecohabitation.com/discussions/2600/sante-champs-electromagnetique-et-plancher-radiant/)
    - [flextherm](https://flextherm.com/en/)

Plancher chauffant hydraulique:
- sur dalle de liège:
    - [snic-liege](https://www.snic-liege.com/type/isolation-liege-plancher-chauffant/)
    - [batiproduits](https://www.batiproduits.com/fiche/produits/plancher-chauffant-sur-dalle-de-liege-p68926303.html)
- faible épaisseur:
    - [caleosol](https://www.caleosol.fr/)
    - [très basse température](https://www.batiproduits.com/fiche/produits/sol-chauffant-rafraichissant-avec-chape-mince-hau-p68926273.html)
    - [22 mm](https://www.batiproduits.com/fiche/produits/plancher-chauffant-rafraichissant-hydraulique-de-p68926263.html)

Prix moyen pose plancher chauffant hydraulique: 70 à 100 €/m²

Retenu:
- plancher chauffant hydraulique
- pompe à chaleur air/eau
- poêle à bûches

## Chauffage de l'eau

Chauffe-eau électrique:
- besoins électriques?
- coût: 500€ pour 300 L

Chauffe-eau solaire:
- panneau
- barres verticales
- quelle prix?
    : 3 000 € et 7 000 €

Calculer la durée nécessaire pour chauffer l'eau:

```
T = 1.1625 x V x △t/P

T = Temps en heure,

V = Volume d’eau en m3,

△t = Température finale (à atteindre) en degrés Celsius - Température initiale (de départ) en degrés Celsius, soit la différence positive des deux températures,

P = Puissance du réchauffeur en kW (kilowatt).
```

## Panneaux solaires

Types de panneaux:
- panneaux solaires thermiques
- panneaux solaires photovoltaïques
- panneaux photovoltaïques hybrides

Calcul de l'angle d'incidence du soleil:
```
cos(i) = sin(h) . sin(hc) + cos(a - ac) . cos(h) . cos(hc)

i: angle incidence soleil
a: azimut soleil
ac: azimut capteur
h: angle élévation soleil
hc: angle élévation capteur
```

> La puissance d’un panneau solaire s’exprime en watts-crête (Wc) ou kilowatts-crête (kWc). Il mesure généralement 1,70m² et sa puissance est d’environ 300 Watts-Crête, soit 0,3 kWc. Cette puissance « crête » (pas « crêpe », hein !) d’un panneau solaire correspond à la puissance maximale de production électrique qu’il peut délivrer. Il s’agit d’une puissance idéale fournie dans des conditions optimales :
>
> - Un ensoleillement de 1 000 W de lumière/m2.
> - Une température extérieure de 25°C.
> - Une bonne orientation des panneaux et aucun ombrage.
>
> [engie](https://mypower.engie.fr/energie-solaire/conseils/puissance-panneau-solaire.html)

Quelle est la surface de chaque pans du toit?

## Isolation

> Un air sec consomme moins de calorie à chauffer qu’un air humide, et entretien la sensation de bien être, même à 19-20 °C
> Inutile de débourser des fortunes dans l’isolation, avant d’avoir traiter les problèmes d’humidité.

Isolation phonique: règle de la densité, plus le matériau est dense, moins il laisse passer le son.
- masse + ressort + masse

    exemple: 1 plaque fermacell + 1 couche de laine de bois + 1 plaque fermacell

Isolation thermique: des couches d'air isolent.
- l'air emprisonné dans un matériau sert à l'isolation thermique
- plus un matériau est dense, moins il est isolant

    exemple: la pierre isole moins que le pisé

- laine de bois:
    - sarking (toit)
    - cloisons
    - murs en pierre
- [laine de verre isoverre](https://www.isover.fr/guides/la-nouvelle-laine-de-verre-isover)
    - cloisons
    - sous-sol
    - faible isolant phonique

matériaux:
- [liège](https://blog.alsabrico.fr/isoler/2016/07/06/top-meilleurs-isolants-phoniques-ecologiques/)

    pour 10 mm:
    - Densité 320kg/m3 +/- 10%
    - Réduction des sons (impacts) 20db (Moyenne)
    - Réduction des sons (aériens)
        - Aigus : 82% ;
        - Médiums : 52% ;
        - Basses 12%
    - Résistance au charges > 5 Tonnes au m²
    - Conductivité thermique (Lambda = 0,042 W / m°C) R= 0,24
    - Résistance au Feu M2

    pour 40 mm:
    Densité 260kg/m3 +/- 10%
    Réduction des sons (impacts) 34db (Moyenne)
    Réduction des sons (aériens)
        - Aigus : 100% ;
        - Médiums : 99%;
        - Basses 32%
    Résistance au charges > 5 Tonnes au m²
    Conductivité thermique (Lambda = 0,042 W / m°C)  R = 0,95
    Résistance au Feu M2

grandeurs pour l'isolation phonique:
- indice d’affaiblissement des bruits aériens (voix, sons d’un réacteur, musiques, etc.) représenté par le signe Rw et quantifié en dB. Plus le décibel est important, mieux ce sera.
- indice de dissipation des bruits de chocs représenté par le signe Δ Lw et exprimé aussi en dB. Ce sont notamment les chutes d’objet, le déplacement des meubles, etc.

## Cloisons

matériaux:
- fermacell
- [placo habito](https://www.placo.fr/Solutions/Innovations-et-produits-phares/Innovations-Placo-R/Habito-R-la-plaque-la-plus-solide-du-marche)
    - résistance avec vis à bois: 20kg
    - résistance avec cheville: 60kg

- fermacell isolant phonique et très résistant
- placo phonique aussi cher que fermacell pour une résistance moindre

composition d'une cloison:
- 1 plaque fermacell
- de l'isolant (laine de bois?)
- 1 plaque fermacell

## Salle de bains

- baignoire java 1.70 m * 0.75 m
- douche
- éviers

## Plancher

[parquet](https://www.manomano.fr/conseil/parquet-contrecolle-parquet-stratifie-et-parquet-massif-les-differences-6007)
- parquet:
    - stratifié
    - contrecollé (ou flottant)
    - massif
- pose:
    - clouée:
        - incompatible avec plancher chauffant
        - bruyant lorsque l'on marche dessus
    - flottante:
        - convient à tous les types de parquet
    - collée:
        - uniquement pour parquet massif ou contrecollé

complexe de plancher:
- parquet
    - massif
        - épaisseur:
            - 20mm?
    - contrecollé
            - 15mm?
    - stratifié
        '- épaisseur:
            - 8mm
- [panneau acoustique fibre de bois](https://www.materiaux-naturels.fr/produit/400-steico-isorel-panneau-acoustique-fibre-de-bois)
    - épaisseur:
        - 8mm
        - 10mm
        - 19mm
- fermacell:
    - [plaque seule](https://www.materiaux-naturels.fr/produit/425-plaque-fermacell-pour-sol)
        - épaisseur:
            - 20mm
            - 25mm
        - [densité](https://www.fermacell.com/en/products/fibre-gypsum/flooring-elements): 1150 kg/m3
    - [plaque avec fibre de bois](https://www.materiaux-naturels.fr/produit/167-plaque-sol-fermacell-avec-fibre-de-bois)
        - épaisseur:
            - 30mm (20mm fermacell + 10mm fibre de bois)
- liège
    - [rouleau](https://www.materiaux-naturels.fr/produit/820-rouleau-de-liege-naturel-)
    - [panneaux isolants](https://www.materiaux-naturels.fr/produit/965-panneau-isolant-de-liege-expanse-naturel-isocor)
        - [isolation phonique pour 10mm](https://www.easy-liege.fr/contenu/37-liege-acoustique-caoutchouc-fiches-techniques):
            - 500 Hz: -18 dB
            - 1000 Hz: -23 dB
            - 5000 Hz: -37 dB
    - [panneaux prélambourdés](https://www.materiaux-naturels.fr/produit/902-panneau-de-liege-prelambourde-4cm)

Pose plancher OSB:

```
DTU 51.3 Planchers en bois ou en panneaux à base de bois


Domaine d’application

 

Le DTU 51.3 « Planchers en bois ou en panneaux à base de bois » donne les spécifications de mise en œuvre des planchers en bois ou en panneaux dérivés du bois, à savoir des ouvrages horizontaux plans et continus porteurs ou non, réalisés sur un ouvrage de structure, qu’ils soient laissés à l’état naturel ou destinés à recevoir un revêtement de sol ou une finition de surface.

 

Le DTU 51.3 s’applique :

    aux travaux neufs ou de rénovation ;
    à tous types de locaux (habitation, local commercial, bureau, entrepôt, ERP, etc.).

 

Le DTU 51.3 ne traite pas :

    des éléments porteurs supportant le plancher ;
    des locaux à très forte hygrométrie ou à risque important de ré-humidification.

 

La version en vigueur de ce DTU, à la publication de cette fiche, est celle de novembre 2004.

 

Matériaux visés

 

Les exigences que doivent respecter les matériaux nécessaires à la mise en œuvre des planchers en bois ou en panneaux à base de bois (matériaux en bois massif ou en panneaux à base de bois, matériaux d’isolation, colles, fixations, etc.) sont précisées dans la partie 1-2 « Critères généraux de choix des matériaux » du DTU 51.3.

 

Mise en œuvre : l’essentiel

 

L’exécution des ouvrages dépendra de la catégorie de planchers à mettre en œuvre :

    planchers porteurs sur solivage mis en œuvre à l’abri de l’eau ;
    planchers porteurs sur solivage mis en œuvre avec risque d’exposition à l’eau ;
    planchers sur lambourdes ;
    planchers de doublage ;
    planchers flottants en panneaux dérivés du bois sur supports continus.

 

Planchers porteurs sur solivage mis en œuvre à l’abri de l’eau

 

Ce type de plancher :

    peut assurer une fonction de contreventement ;
    est posé sur une structure discontinue (solivage en bois, métal, béton, etc.) dont la mise en œuvre doit être conforme aux règles de l’Art ;
    reçoit, en général, un revêtement de sol ou une finition de surface formant une couche d’usure et décorative.

 

La planéité du plancher fini est conditionnée par celle du support initial. L’entrepreneur portera donc une attention particulière sur ce point avant mise en œuvre du plancher en bois.

 

Avant mise en œuvre, les supports doivent respecter une certaine humidité selon leur nature (bois ou maçonnerie).

 

Les stockages prolongés sur chantier sont à éviter et doivent permettre de conserver une humidité des bois ou panneaux la plus proche possible de celle de service.

Les lames du plancher :

    doivent reposer sur 3 appuis minimum avec un recouvrement minimal sur les appuis de 18 mm ;
    ont une épaisseur définie en fonction de l’entraxe des solives et des charges à prendre en compte ;
    sont mises en œuvre bord à bord et à joints décalés.

 

Le revêtement de sol sera ensuite disposé à bord jointif ou avec un jeu de 1 à 1,5 mm/m entre chaque panneau, selon sa nature.

 

La fixation peut être faite par clouage, agrafage ou vissage. Quel que soit le mode de fixation, les pointes ou vis seront :

    espacées de 150 mm maximum sur les appuis périphériques ;
    espacées de 300 mm maximum en partie courante ;
    à une distance minimale de 8 mm des rives.

 

En cas de clouage, un vissage complémentaire est nécessaire aux 4 angles du panneau et à mi-longueur.

 

Selon les indications des documents particuliers du marché (DPM), des zones de fractionnement et une aération peuvent être à prévoir.

 

Planchers porteurs sur solivage mis en œuvre avec risque d’exposition à l’eau

 

Ce type de plancher reçoit, en général, un revêtement de sol ou une finition de surface formant une couche d’usure et décorative.

 

Sa mise en œuvre est sensiblement la même que celle des planchers porteurs visés précédemment, à l’exception des points ci-dessous :

    présence indispensable d’une coupure de capillarité en cas de fondations en béton ou en maçonnerie ;
    si vide sanitaire : exigence d’une hauteur minimale de 30 cm sous le solivage ou le support bois le plus bas, avec absence de toute matière organique et orifices de ventilation correctement dimensionnés ;
    quel que soit le mode de fixation, cette dernière se fait en deux temps : 1/3 lors de la mise en place, le reste après la mise hors d’eau et le séchage du bâtiment mais avant la pose du revêtement de sol.

 

Planchers sur lambourdes


Ce type de plancher est réalisé de manière semblable aux planchers porteurs sur solivage mis en œuvre à l’abri de l’eau et reçoit, en général, un revêtement de sol ou une finition de surface formant une couche d’usure et décorative.

 

Les lambourdes peuvent être :

    collées, clouées ou scellées sur la structure porteuse ;
    désolidarisées par une sous-couche de répartition (constitution d’un plancher flottant) ;
    mises en œuvre sur une structure porteuse en bois, maçonnée ou métallique.

 

L’ensemble des lambourdes doit être mis en œuvre avec un écartement régulier, de manière alignée et avec des joints décalés d’une rangée à l’autre.

 

Planchers de doublage

 

Ce type de plancher :

    reçoit, en général, un revêtement de sol ou une finition de surface formant une couche d’usure et décorative ;
    est toujours réalisé à l’abri de l’eau ;
    n’assure pas la fonction porteuse, cette dernière étant assurée par un ouvrage sous-jacent.

 

Selon les cas, leur mise en œuvre sur paroi porteuse continue en maçonnerie sera semblable à celle des planchers sur lambourdes ou celle des planchers flottants en panneaux à base de bois.

 

Les planchers de doublage peuvent être mis en œuvre sur des supports :

    en bois dont l’humidité ne dépasse pas 10% :
        les panneaux sont assemblés par rainure et languette, sur les 4 rives ;
        les panneaux sont posés à joints transversaux alternés en laissant un espace d’une dizaine de mm en périphérie de la pièce, ultérieurement caché par une plinthe ;
        en périphérie, les panneaux sont fixés à l’aide de vis ou de pointes disposées de 20 à 30 mm de leur bord et espacées de 15 à 20 cm d’une vis à chaque angle et de 40 à 50 cm en partie courante ;
    métalliques secs :
        une couche de désolidarisation est placée entre les éléments porteurs métalliques et les panneaux de doublage ;
        les panneaux sont mis en œuvre à joints transversaux alternés, sans jeu et collés ;
        un espace d’une dizaine de mm est laissé en périphérie de la pièce.

 

Planchers flottants en panneaux à base de bois

 

Ce type de plancher est également mis en œuvre à l’abri de l’eau et n’assure pas de fonction porteuse (assurée par la présence d’un ouvrage sous-jacent). Il reçoit obligatoirement un revêtement de sol ou une finition de surface formant une couche d’usure et décorative.

 

Le plancher flottant est mis en œuvre sur un support composé d’une forme d’égalisation et/ou de désolidarisation en vrac (si nécessaire selon inégalités et planéité générale de la structure porteuse).

 

Les panneaux sont :

    assemblés par rainure et languette vraie ou fausse ;
    posés à joints transversaux alternés ;
    collés en laissant un jeu en périphérie de la pièce.
```

## Escalier

> **Formules:**
> Longueur = nombre_marches * profondeur_marche
> Surface = largeur_marche * longueur
> Nombre de marches = hauteur_disponible / hauteur_marche
> Confort = hauteur_marche * 2 + profondeur_marche

<div style="margin-bottom: 1em">
    <form id="stair-calculator" action="#">
        <div class="field">
            <label class="label" for="available-height">Hauteur</label>
            <div class="control">
                <input type="text" class="input available-height" value="2.73">
            </div>
        </div>
        <div class="field">
            <label class="label" for="available-width">Largeur</label>
            <div class="control">
                <input type="text" class="input available-width" value="0.9">
            </div>
        </div>
        <div class="field">
            <label class="label" for="step-height">Hauteur marche</label>
            <div class="control">
                <input type="text" class="input step-height" value="0.18">
            </div>
        </div>
        <div class="field">
            <label class="label" for="step-depth">Profondeur marche (giron)</label>
            <div class="control">
                <input type="text" class="input step-depth" value="0.26">
            </div>
        </div>
        <div class="control">
            <button type="submit" class="button is-primary">ok</button>
        </div>
    </form>
    <div class="result"></div>
</div>

<script>
function getStepsCount(availableHeight, stepHeight) {
    return availableHeight / stepHeight;
}

function getStairLength(stepCount, stepDepth) {
    return stepCount * stepDepth;
}

function getStairSurface(stairLength, stairWidth) {
    return stairLength * stairWidth;
}

function measureStair(availableHeight, stairWidth, stepHeight, stepDepth) {
    const stepsCount = getStepsCount(availableHeight, stepHeight);
    const stairLength = getStairLength(stepsCount, stepDepth);
    const stairSurface = getStairSurface(stairLength, stairWidth);

    return {
        stairSurface: stairSurface.toFixed(2),
        stairLength: stairLength.toFixed(2),
        stairWidth: parseFloat(stairWidth).toFixed(2),
        stepsCount: stepsCount.toFixed(1),
    };
}

function getStepComfort(stepHeight, stepDepth) {
    return stepHeight * 2 + stepDepth;
}

function isStepComfortable(stepHeight, stepDepth) {
    const comfort = getStepComfort(stepHeight, stepDepth);

    return 0.6 <= comfort && comfort <= 0.65;
}

document.querySelector('#stair-calculator').addEventListener('submit', function (event) {
    event.preventDefault();
    const availableHeight = parseFloat(document.querySelector('.available-height').value);
    const availableWidth = parseFloat(document.querySelector('.available-width').value);
    const stepHeight = parseFloat(document.querySelector('.step-height').value);
    const stepDepth = parseFloat(document.querySelector('.step-depth').value);

    const result = measureStair(
        availableHeight,
        availableWidth,
        stepHeight,
        stepDepth,
    );

    document.querySelector('.result').innerHTML = `
        <pre>
            Longeur: ${result.stairLength}
            Largeur: ${result.stairWidth}
            Surface: ${result.stairSurface}
            Nombre de marches: ${result.stepsCount}
            Comfort (entre 0.60 et 0.65): ${getStepComfort(stepHeight, stepDepth).toFixed(2)}

            ${isStepComfortable(stepHeight, stepDepth) ? '<span class="has-text-success">Escalier confortable</span>' : '<span class="has-text-danger">Escalier inconfortable</span>'}
        </pre>
    `;
})
</script>

## Electricité

bio rupteur ou bio commutateur ou interrupteur automatique de champ:
- [IAC Gigahertz Solutions NA7 Comfort](https://www.electromagnetique.com/produit/se_proteger/electricite_biocompatible/iac/iac-gigahertz-solutions-na7-comfort/)
- [IAC Gigahertz Solutions NA8 Comfort](https://www.electromagnetique.com/produit/se_proteger/electricite_biocompatible/iac/iac-gigahertz-solutions-na8-comfort/)
- [explications pic bleu](https://www.picbleu.fr/page/iac-interrupteur-automatique-champ-courant-champs-electromagnetiques)
- [biorupteur II](https://www.geotellurique.fr/interrupteurs-automatiques-de-champs/904-biorupteur-ii-a-coupure-bipolaire-pso.html)

prises électriques
- [a quelle hauteur installer une prise électrique?](https://www.legrand.fr/questions-frequentes/a-quelle-hauteur-installer-une-prise-electrique-ou-prise-de-courant)

    > Pour être en conformité avec la norme NF C 15-100 :
    > - Pour les prises électriques de 16A (c'est-à-dire délivrant une intensité maximale de 16 Ampères) et 20A, le centre des alvéoles (ou trous) doit se trouver au minimum à 5 cm au-dessus du sol fini (plancher, carrelage, moquette...)
    > - Pour les prises de courant 32A, cette hauteur minimale est de 12 cm
    > - Pour toutes ces prises (16A, 20A ou 32A), la hauteur maximale autorisée est de 1,30 m
    >
    > Dans votre salle de bain, la hauteur d'installation d'une prise électrique est comprise entre 0,90 et 1,30 m.

## Réseau

prise RJ45 extérieur
- [toolstation](https://www.toolstation.fr/search?q=prise%20rj45)

emplacement de la box:
- au niveau du tableau électrique

téléphone fixe:
- utiliser un câble rj11 mâle vers rj45 mâle entre la box et le réseau, puis un adaptateur rj45 mâle vers rj11 femelle entre le réseau rj45 et le téléphone
- trouver un téléphone se branchant directement sur une prise rj45

> quelle différence entre routeur wifi et point d'accès wifi?

routeur wifi sur rj45 si besoin:
- [Tenda AC8](https://www.amazon.fr/dp/B07Z7NY23Q/?tag=routeur-wifi-conso-21)

## Démolition

Démolir 2 dalles de béton au marteau piqueur

location possible:
- [lokastar](https://www.lokastar.com/location-materiel-chantier-btp?search=marteau%20piqueur&cm=0#tlb)
    - est-ce qu'un burin est fourni en plus de la pointe hexagonale?
    - pneumatique: nécessite de louer le compresseur de chantier avec?
- [loxam 27kg](https://www.loxam.fr/p/brisebeton-hte-freq-27kg/002-0025-010420#)

achat possible:
- [Marteau piqueur Einhelll TC DH 43](https://www.youtube.com/watch?v=z9ADF1c0Sic)
    - 14 - 21 kg
    - 1600W - 43J
    - [leroymerlin](https://www.leroymerlin.fr/produits/outillage/outillage-electroportatif/perforateur-burineur-marteau-piqueur/marteau-piqueur/marteau-demolisseur-tc-dh-43-82198485.html)
- [1600W](https://www.leroymerlin.fr/produits/outillage/outillage-electroportatif/perforateur-burineur-marteau-piqueur/marteau-piqueur/marteau-piqueur-hexagonal-scheppach-ab1600-1600-w-70603925.html)
- [1900W](https://www.leroymerlin.fr/produits/outillage/outillage-electroportatif/perforateur-burineur-marteau-piqueur/marteau-piqueur/marteau-piqueur-hexagonal-scheppach-ab1900-1900-w-82257651.html)

[40 joules pour 1500 W](https://www.youtube.com/watch?v=kq3YmlxlT0U)
[exemple](https://duckduckgo.com/?t=ffab&q=marteau+piqueur&iax=videos&ia=videos&iai=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D4-W3YiLpiSk)

achat réalisé: [leroymerlin](https://www.leroymerlin.fr/produits/outillage/outillage-electroportatif/perforateur-burineur-marteau-piqueur/marteau-piqueur/marteau-demolisseur-tc-dh-43-82198485.html)

## Vocabulaire

- dalle en béton de chaux
- enduit chaux/chanvre
- plaques de fermacell
- frein-vapeur
    : limite le passage de l'humidité, empêche le passage de l'air
- pare-vapeur
    : empêche le passage de l'humidité et de l'air
- verre cellulaire
    : mélange de verre recyclé et de matières minérales contenant des bulles de CO2

    > - C’est un matériau étanche qui est idéal pour isoler des environnements humides. C’est l’un des seuls matériaux isolants qui n’est pas du tout affecté par l’humidité.
    > - Il n’absorbe donc pas l’humidité et ne se dilate pas.
    > - C’est aussi un excellent pare-vapeur.
    > - Le verre cellulaire est incombustible. Il est d’ailleurs classifié dans la catégorie A1, la plus sûre.
    > - Comme c’est un matériau inorganique, il est imputrescible et résistant à la vermine.
    > - Il possède une longue durée de vie et garde ses propriétés isolantes pendant plus de 50 ans.
    > - C’est un matériau écologique et recyclable.
    > - Le verre cellulaire est léger et résistant à la pression.
    > source: [isolation-info](https://www.isolation-info.fr/materiaux-isolants/verre-cellulaire)
- faîtage
    : sommet du toit
- BSO
    : volet brise-soleil orientable
- ECS
    : eau chaude sanitaire

## Questions

- sens d'ouverture des fenêtres: vers l'intérieur
- types de volets: volets roulants électriques ou manuels à l'étage?
- volet bois pour la porte Nord, volets brise-soleil orientables pour les fenêtres du rez-de-jardin et la baie vitrée Sud, volet bois pour le bureau du rez-de-jardin
- baie vitrée Sud: 3 pans à battant, ouverture vers l'intérieur si assez d'espace disponible, serrure et poignée cylindrique pour l'un des battant?
- sol rez-de-jardin: travertin ou tomettes?
- sol salle de bain: carrelage, bois, PVC, liège, combinaison sous-couche carrelage + surcouche bois?
- chauffage au sol électrique + poële à bois avec système de génération d'électricité par la chaleur?
- quels interrupteurs utiliser? bascule?
- où passer les gaines électriques, les canalisations ?
- Quelle puissance est nécessaire pour chauffer la maison ?

Marteau piqueur:

huile 45 ml de SAE 15W40

## Réunion de chantier

**maçon + plaquiste**
- phasage travaux pose isolation et escalier: dans quel ordre?
- comment poser l'isolant sous plancher Est sur vide technique?

**menuisier + plaquiste**
- quelle configuration des portes de l'escalier du RDC à adopter? les 2 au même niveau? retrait de la porte menant à l'étage?
- quelle configuration des cloisons et rampes de l'escalier?

**plaquiste**
- comment combler le sommet des cloisons de l'étage?
- comment positionner les plaques?

**électricien + charpentier**
- éclairage du RDC et de l'étage:
    - quel type?
    - où passer les fils?

**électricien + plaquiste**
- VMC:
    - où positionner le bloc?
    - où positionner les conduits?
        - dans un caisson?
        - dans un faux plafond?
        - via le vide technique?
    - si caisson, est-ce possible de le positionner contre la poutre faîtière?
    - est-ce possible de positionner les bouches d'aération du WC du RDC et de la SDB dans un mur plutôt qu'au plafond? (pour pouvoir passer le conduit dans le vide technique Est)

**charpentier + plombier**
- où positionner les canalisations du WC de l'étage?

**charpentier + maçon**
- où faire sortir les aérations des hérissons ventilés? murs? toiture?

**charpentier + menuisier**
- comment combler le sommet de la baie vitrée? poutre? bardage bois? autre?

**maçon + plombier**
- comment poser le plancher? laisser des espaces libres ou tout couvrir puis découper ensuite?
- planning de la pose du conduit du poêle: quand? laisser un espace libre dans le plancher?

**terrassier + charpentier**
- où placer le conduit d'aération de l'ANC?
    - dans un caisson à l'intérieur contre l'isolant?
    - à l'extérieur le long de la gouttière Nord-Ouest puis sous l'avancée du toit de la façade Ouest?
    - au niveau de la fosse?

**général**
- phasage des travaux: dans quel ordre chaque artisan va intervenir?
- planning des travaux: quand chaque artisan va intervenir?
- quand commander les éléments? (pompe à chaleur...)

## sol

- SDB et WC: carrelage imitation travertin
- tomettes sous poële jusqu'au mur Sud

## bois intérieur

vernis == vitrificateur

faire 3x pour du vernis polyuréthane, 2x pour du vernis acrylique
1. poncer 80-180 240
1. passer un coup de chiffon
1. passer un coup d'éponge humide, attendre que ça sèche, poncer 120 (optionel)
1. appliquer un fond dur pour saturer les pores (optionel)
1. appliquer 1 couche de vernis


- [vernir](https://www.youtube.com/watch?v=BzovptkWJwE)
- [comment vitrifier](https://www.youtube.com/watch?v=tVUXmmlLlBk)
- [finitions](https://www.youtube.com/watch?v=oHO5E-kwUpg)

éviter l'essence de térébentine qui est neurotoxique

huile de tung:
1. 2 couches bien essuyées

## outils

- [projecteur](https://www.amazon.fr/Projecteur-HYCHIKA-Ultra-Lumineux-Ext%C3%A9rieur-Construction/dp/B091Q2N3GT/ref=sr_1_5?keywords=projecteur+chantier&qid=1665516732&qu=eyJxc2MiOiI2LjYxIiwicXNhIjoiNi4xNCIsInFzcCI6IjUuNzMifQ%3D%3D&sr=8-5)
- [ponceuse](https://www.amazon.fr/dp/B08GKWXXM5/?coliid=IGZNM4OGVDZ0C&colid=9ON2DVFJYCNU&psc=0&ref_=lv_ov_lig_dp_it)