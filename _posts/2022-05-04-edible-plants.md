---
layout: post
author: nicolas
category: plants
---

<!-- excerpt end -->

- [nettle](https://www.wildwalks-southwest.co.uk/when-not-to-eat-nettles/)

    > John Wright (from his Hedgerow book)
    >
    > 'At the first sign of flowers you must stop picking. The plant will now start producing cystoliths - microscopic rods of calium carbonate - which can be absorbed by  the body where they will mechanically interfere with kidney function.'

    green juice of the day:
    - apple
    - carrot
    - nettle
