---
layout: post
author: nicolas
title: Recettes de cuisine
category: cooking
---

- Financier

<!-- excerpt end -->

## crêpes au sarrasin

> [Source](https://www.carnetdescapades.com/recettes/galettes-sarrasin-bretonnes.html)

1. mélanger dans un saladier:
    - 250g de farine de sarrasin
    - 1 CS rase de gros sel
    - 1 oeuf
    - 550mL d'eau froide
1. laisser reposer 1h avec un torchon posé sur le saladier
1. graisser une poêle
1. faire chauffer la poêle
1. verser la pâte

## houmous

1. mixer 3 cs huile d'olive, 3 cs huile de sésame, 1/2 citron, 1 gousse d'aïl, 1 petite boîte de conserve de pois chiches, sel, poivre, herbe sauvage (lierre terrestre, gaillet mou...)

## mousse au chocolat - pois chiches

1. faire fondre 150g de chocolat noir
2. battre 150mL de jus de pois chiche en neige (le jus d'une boite de pois chiches de 400g) dans un saladier
3. ajouter 60g de sucre dans le saladier
4. verser le chocolat fondu dans le saladier
5. mélanger et laisser au frigo > 4 heures

## mousse au chocolat - oeufs

1. fondre 200g chocolat
1. ajouter 5 jaunes d'oeufs
1. battre les blancs en neige
1. mélanger les ingrédients
1. laisser >= 3h au frigo

## cake salé

> cake moelleux et aéré, qui colle au moule.

1. couper et cuire à la vapeur douce (vitaliseur de Marion) une courgette et un morceau de patate douce
1. mélanger 3 oeufs, 150g farine de sarrasin, 30g maïzena, 1 sachet de levure, 1 pincée de sel, poivre, 2 cs huile d'olives et ajouter la courgette et la patate douce préalablement mixées
1. verser le mélange dans le moule graissé du vitaliseur
1. cuire entre 30' et 1h dans le vitaliseur

## cake au butternut

1. râper puis broyer avec un extracteur de jus 300g de butternut
2. mélanger dans un saladier:
    - les 300g de butternut
    - 100g farine de sarrasin
    - 1 cc de bicarbotante de soude diluée dans un peu d'eau chaude ou 1 sachet de levure
    - 3 oeufs
    - 60g de beurre
    - soit:
        - des oignons et des lardons cuits à la poêle
        - du thon mélangé à des pommes de terre cuites à la vapeur douce
3. verser dans un moule à cake
4. cuire > 30' à 150 °C

## Mayonnaise

> [Source](https://www.grands-meres.net/mayonnaise/)

1. mettre 2 jaunes d’oeufs dans un bol
1. ajouter dans le bol:
    - une cuillère à café de moutarde
    - une cuillère à café de vinaigre
    - une pincée de sel
    - une pincée de poivre
1. fouetter le tout pendant une minute
1. verser 15 cl d’huile doucement en petit filet tout en fouettant continuellement. La mayonnaise va s’épaissir petit à petit.

avec les 2 blancs d'oeufs, faire un financier:

## Financier

> [Source](https://www.hervecuisine.com/recette/recette-des-financiers-aux-amandes-facile-et-inratable/)

1. fondre 125g de beurre dans une casserole
1. mélanger dans un saladier:
    - 125g de sucre
    - 50g de farine
    - 125g de poudre d'amandes
    - une pincée de sel
1. ajouter 4 blancs d'oeuf séparément en mélangeant à chaque fois
1. ajouter le beurre fondu
1. mettre au four (15 minutes à 230°C)

Avec 2 oeufs:
1. fondre 63g de beurre dans une casserole
1. mélanger dans un saladier:
    - 63g de sucre
    - 25g de farine
    - 63g de poudre d'amandes
    - une pincée de sel
1. ajouter 2 blancs d'oeuf séparément en mélangeant à chaque fois
1. ajouter le beurre fondu
1. mettre au four (15 minutes à 230°C)

## cookies

1. mélanger dans un saladier avec une marise:
    - 120g beurre à température ambiante
    - 100g sucre
1. ajouter:
    - 1 oeuf
    - 185g farine de châtaigne
    - 1 cc bicarbonate de soude diluée dans un peu d'eau chaude
    - 1 cc sel
    - 1 cs maïzena
    - [1 sachet de sucre vanillé]
1. former 14 boules à déposer sur une plaque recouverte de papier cuisson
1. écraser un peu chaque boule
1. cuire 15' à 150 °C
1. sortir la plaque du four et laisser refroidir pour que les cookies durcissent

## palets bretons sans gluten

1. écraser 100g de beurre pour avoir une consistence de pommade
1. dans un saladier mélanger 2 jaunes d'oeufs et 80g de sucre
1. ajouter le beurre, 2 pincées de sel, 130g de farine de riz, 1 cc bicarbonate de soude ou poudre à lever
1. mettre la boule de pâte 2 heures au frigo enroulée dans du film alimentaire
1. sortir et aplatir la boule à 2 cm d'épaisseur entre 2 morceaux de film alimentaire
1. découper des cercles de 5 cm de diamètre avec un verre ou un cercle en inox et les déposer sur une plaque recouverte de papier cuisson ou dans des moules (laisser les emportes pièces dans le four pour avoir des bords nets)
1. cuire au four 15 minutes à 170 °c
