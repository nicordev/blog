---
layout: post
author: nicolas
title: Sites de plomberie
category: benchmark
---

Voici quelques exemples de sites

<!-- excerpt end -->

# Plomberie - benchmark

## [plombier2saintetienne](https://www.plombier2saintetienne.fr/)

Les + :
- design cohérent
- numéro de téléphone visible
- page pour chaque prestation
- témoignages clients

Les - :
- scroller pour voir le numéro de téléphone sur mobile
- trop de texte sur la page d'accueil
- page cassée pour les fuites et le formulaire de contact

## [selfcity](https://www.selfcity.fr/plombier-toulouse)

Les + :
- design cohérent
- tarifs
- équipe
- carte
- prestations détaillées
- lien avec numéro de téléphone
- système de prise de rdv
- navigation mobile aisée avec les boutons pour appeler et prendre rdv

Les - :

## [bruno & fils](https://www.bruno-fils.be/)

Les + :
- lien avec numéro de téléphone
- prestations
- témoignages clients
- formulaire de contact, email, adresse

Les - :
- pas responsive
- design
- police trop petite
- animations trop lentes

## [sabeko lyon](https://www.sabeko.fr/plomberie/)

Les + :
- lien avec numéro de téléphone
- prestations

Les - :
- trop de texte
- informations de contact peu visibles

## [vdplomberie](https://www.vdplomberie.fr/services-de-plomberie/installation-sanitaire/)

Les + :
- possibilité de se faire rappeler
- grand formulaire de contact

Les - :
- prestations peu visibles
- texte du formulaire de contact peu lisible

## [plomby](https://plomby.be/)

Les + :
- design épuré

Les - :
- témoignages clients écrasés

## [morel](https://www.morel-plombier.com/)

## [leroy](https://www.ets-leroy-depannages.fr/plombier-installation-depannage/)

## [furlanjean-pierre](https://www.furlanjean-pierre.com/plomberie)

Les - :
- cookies
- pas responsive