---
layout: post
author: nicolas
title: Recettes de jus
category: cooking
---

<!-- excerpt end -->

à mettre dans l'extracteur dans cet ordre:
1. fruit ou légume fruit pour 'huiler' l'extracteur (pomme, poire, citron, poivron, tomate...)
1. le gingembre
1. légumes feuille et/ou feuilles plantes sauvages (épinards, roquette, plantain, pissenlit, ortie, chardon...)
1. légume plus dur et/ou racine des plantes sauvages en dernier pour tout pousser (carotte, pomme de terre, fenouil...)

jus:
- carotte
- pomme
- épinards

jus:
- branche de fenouil x3
- pomme de terre x1
- carotte x3
- pissenlit
- gingembre x1 morceaux
- poire x1

jus:
- branche de fenouil x3
- pomme de terre x1
- gingembre x1 morceaux
- poire x1
