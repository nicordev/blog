---
layout: post
author: nicolas
excerpt_separator: <!-- excerpt end -->
---

Some energetic tips I'm using to find some balance.

<!-- excerpt end -->

## Along the day

- finger tapping on the head, body, under hand short side when emotions become too strong (Emotional Freedom Technic)
- breath in, stop with full lungs, visualize golden white energy inside and around me like a gold bubble wrapped in a silver mirror that will reflect all negativity and transmute it into pure light
- breath while focusing on my hips

## Daily routine

- ground myself by feeling roots extending under my feet that go deep to the center of the Earth, see a ray of light from by base to the center of the Earth, focusing on my body sensations, thanking our ancestors.
- think white
- ask my whole being with the help of guides, archangels (Mickaël seems the more appropriate) to cut all toxic links with humanity's shadow (ego, eugenism...)
- leave the ego, leave the control
- choose peace, love, light, joy, life, family, freedom
- now that there is more room in my being, choose to embody my soul
- one or two Qi-Gong movements repeated 8 times

## Sometimes

- on each 7 main chakra, breath in deeply then let go 3 times while focusing on the current chakra (feeling and color) + on 3 first energetic bodies, then stop breathing on empty lungs, wait as long as possible, breath in deeply, then stop on full lungs during 15 seconds, then let go (Wim Hof method with little customization)
- meditation (stay still, focus on breathing, focus on each part of by body one at a time, observe my thoughts, feelings)

## How to pray

- say out loud or in my head or write simple yet precise sentences formulated in present tense in affirmative form, like so:

    > I choose to enjoy this day whatever the outer circumstances

## To escape the Karpman drama triangle

I'm neither victim, persecutor nor rescuer, only responsible.

> [post](https://traqq.com/blog/the-drama-triangle-what-is-it-and-how-do-you-escape-it/)
