---
layout: post
author: nicolas
title: Bases d'escalade
categories: mountain
tags:
- fr
---

<!-- excerpt end -->

## Moulinette

- 2 baudriers
- 2 casques
- 1 longe
- 2 mousqueton à vis
- 1 reverso
- 1 corde à simple (dynamique)
    - diamètre 10 mm
    - longueur 70 m
- 12 dégaines

## Descente en rappel

- 1 baudrier
- 1 casque
- 1 longe double
- 2 mousqueton à vis
- 1 reverso
- 1 ficelou
- 1 corde statique ou dynamique
    - longueur 2x la hauteur à descendre
