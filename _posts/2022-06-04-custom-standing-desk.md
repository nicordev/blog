---
layout: post
author: nicolas
title: Bureau debout fait maison
tags:
- fr
---

<!-- excerpt end -->

fixation plateau
- [équerre renforcée 0.04 * 0.04 * 0.06 m](https://www.manomano.fr/p/equerre-dassemblage-acier-galvanise-40x40x60-ep-2mm-23823274): 0.77 €

    ![](https://cdn.manomano.com/images/images_products/261713/P/24120119_1.jpg)
- [équerre renforcée 0.09 * 0.09 * 0.06 m](https://www.manomano.fr/p/equerre-mixte-dassemblage-avec-renfort-waelbers-e2-90x90x60mm-epais25mm-490411-36625416): 1.24 € (0.86 €)
- [équerre renforcée 0.07 * 0.07 * 0.055 m](https://www.manomano.fr/p/equerre-perforee-renforcee-zinguee-70-x-70-x-55-x-25-1102103): 1.31 €

pieds
- [pied réglable M8 avec écrou griffe à enfoncer](https://www.manomano.fr/p/4x-vis-pied-reglable-m8-meuble-chaise-ajustable-ecrou-griffe-a-enfoncer-verin-nivellement-49098046): 9 €

    ![](https://cdn.manomano.com/images/images_products/17962593/P/50754743_3.jpg)
- [vérins M10 Emuca pour meubles](https://www.manomano.fr/p/lot-de-20-vernis-circulaire-m10-pour-pied-de-meuble-emuca-d-23-a-reglage-interieur-h-46-mm-33305552?product_id=9827453): 26 €

## divers

vis à bois
- [M5](https://www.manomano.fr/catalogue/p/100x-vis-cruciforme-m5-x-25mm-filet-complet-acier-pozidrive-bois-meuble-53008996)
- [coffret varié](https://www.manomano.fr/p/coffret-de-vis-galvanisees-a-tete-fraisee-780-pcs-2526462)

support pour visser des boulons:
- [douilles](https://www.manomano.fr/douille-2210)

vis avec tête hexagonale:
- [tire-fond](https://www.manomano.fr/tire-fond-1120)