---
layout: post
author: nicolas
excerpt_separator: <!-- excerpt end -->
---

<!-- excerpt end -->

## Adverbs of time

We must use them like the LOW order, where LOW stands for:
1. how long?
1. how often?
1. when?

> I've been running for 3 years, 2 times a week when it's raining.

## Tenses

Past simple to describe the context:

> I had an interview, then I got the job

Past perfect to emphasize the subject:

> Finally, I have had an interview!

